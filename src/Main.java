import entities.Etapa2;
import entities.Etapa3;

public class Main {
    public static void main(String[] args) {
        System.out.println("ETAPA 1");

        Etapa2 etapa2 = new Etapa2();
        etapa2.printEtapa2State();

        Etapa3 etapa3 = new Etapa3();
        etapa3.printEtapa3State();
    }
}
